#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from collections import namedtuple
from datetime import datetime, timedelta

from sh import openssl

TLSAHash = namedtuple('TLSAHash', 'algo record_value')
TLSA_SHA256 = TLSAHash(algo='sha256', record_value='1')
TLSA_SHA512 = TLSAHash(algo='sha512', record_value='2')
AVAILABLE_HASH = {
    1: TLSA_SHA256,
    2: TLSA_SHA512
}

x509_fingerprint = openssl.bake('x509', '-noout', '-fingerprint')
x509_details = openssl.bake('x509', '-noout', '-text', '-certopt', 'ca_default')


DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


def read_zone_file(filename):
    with open(filename) as f:
        ttl = timedelta(seconds=86400)
        current_timestamp = datetime.now()
        keep_current_line = True
        for line in f:
            line = line.strip()
            if line.startswith('$TTL'):
                ttl = timedelta(seconds=int(line[5:]))
            elif line.startswith('; Added at'):
                current_timestamp = datetime.strptime(line[11:], DATE_FORMAT)
                keep_current_line = current_timestamp > datetime.now() - ttl
            
            if keep_current_line:
                print(line)


def get_fingerprint(cert_file, hash_type=TLSA_SHA512):
    output = x509_fingerprint('-{}'.format(hash_type.algo), '-in', cert_file)
    splitted = output.split('=')
    if len(splitted) != 2:
        raise ValueError('Unparseable fingerprint: {}'.format(output))
    return splitted[1].strip().replace(':', '').lower()


def get_hostnames(cert_file):
    hostnames = set()
    is_alternative = False
    for line in x509_details('-in', cert_file):
        line = line.strip()
        if line.startswith('Subject:'):
            hostnames.add(line.split('CN=')[1])
        elif line.startswith('X509v3 Subject Alternative Name:'):
            is_alternative = True
        elif is_alternative:
            is_alternative = False
            for alternative in line.replace(',', '').split():
                alt_type, alt_value = alternative.split(':')
                if alt_type == 'DNS':
                    hostnames.add(alt_value)
    return hostnames


def get_tlsa(cert_file, ports='80', usage='1', selector='0', hash_type=TLSA_SHA512):
    if isinstance(ports, str):
        ports = [ports]
    fingerprint = get_fingerprint(cert_file, hash_type=hash_type)
    tlsa_lines = ['_{port}._{protocol}.{hostname}. IN TLSA {usage} {selector} {hash_type} {value}'.format(
              port=port,
              protocol='tcp',
              hostname=hostname,
              usage=usage,
              selector=selector,
              hash_type=hash_type.record_value,
              value=fingerprint)
        for hostname in get_hostnames(cert_file) for port in ports]
    return tlsa_lines
      
              
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Generate TLSA records for given X509 certificates.')
    parser.add_argument('--cert', required=True, help='Certificate file')
    parser.add_argument('--ports', type=int, action='append', help='Service ports')
    parser.add_argument('--type', choices=['web', 'smtp', 'xmpp'],
                       help='Select type')
    parser.add_argument('--usage', choices=range(0,3), type=int, default=1)
    parser.add_argument('--selector', choices=range(0,1), type=int, default=0)
    parser.add_argument('--hash_type', choices=AVAILABLE_HASH.keys(), type=int, default=2)
    
    args = parser.parse_args()
    print('\n'.join(
            get_tlsa(args.cert,
                     ports=args.ports,
                     usage=args.usage,
                     selector=args.selector,
                     hash_type=AVAILABLE_HASH[args.hash_type]
                     )
             )
         )
