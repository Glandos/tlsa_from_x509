This is work in progress.

For now it only outputs TLSA record on standard output, but the goal is to manage a BIND zone file with simple rollover policy, based on TTL.
Requirements
============
-  Python 3
- [sh](http://amoffat.github.io/sh/)
- openssl binary

Examples
========
Run: `python3 tlsa_from_x509.py --cert my_web_cert --ports 443`

Output:
            
    _443._tcp.sync.example.com. IN TLSA 1 0 2 f7bc7c6a220d18dd37145f1fd0481e7576d32df6793657b5cc3b04337914a402a7b8bd7b8efd83e440f7f4e66df2b88b22d416b320008b698a0cfd58d3a24681
    _443._tcp.glandos.example.com. IN TLSA 1 0 2 f7bc7c6a220d18dd37145f1fd0481e7576d32df6793657b5cc3b04337914a402a7b8bd7b8efd83e440f7f4e66df2b88b22d416b320008b698a0cfd58d3a24681
    _443._tcp.example.com. IN TLSA 1 0 2 f7bc7c6a220d18dd37145f1fd0481e7576d32df6793657b5cc3b04337914a402a7b8bd7b8efd83e440f7f4e66df2b88b22d416b320008b698a0cfd58d3a24681